CREATE DATABASE `dorti` DEFAULT CHARSET utf8;

USE `dorti`;

CREATE TABLE `dorti`.`user` (
  id CHAR(36) PRIMARY KEY DEFAULT uuid(),
  name VARCHAR(255) NOT NULL
);


CREATE TABLE `dorti`.`address` (
  id CHAR(36) PRIMARY KEY DEFAULT uuid(),
  street_name VARCHAR(255) NOT NULL,
  fk_address_user CHAR(36) NOT NULL UNIQUE,
  CONSTRAINT `fk_address_user`
    FOREIGN KEY (`fk_address_user`)
    REFERENCES `dorti`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
