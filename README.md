# getting the tools

Download `docker` and `docker-compose`.

# launching

```sh
docker-compose up
```

# checking logs

```sh
docker logs -f hikari-proto-db
```

> option `-f` is used to see the logs in a continius stream


# connect a client

We reuse the same image to spawn a new container that will be used as a client

```sh
docker run -it --network default --rm mariadb:10.4.5-bionic mysql -h hikari-proto-db -u root
```

> the `--rm` option, ensure that the container is destroyed after being used. So that the stack stays clean
